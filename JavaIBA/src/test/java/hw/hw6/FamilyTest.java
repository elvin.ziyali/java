package hw.hw6;


import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class FamilyTest {
    Family family;
    Human mother;
    Human father;
    Human child1;

    @Before
    public void before() {
        mother = new Human("M", "C", 1999);
        father = new Human("E", "Z", 1999);
        family = new Family(mother, father);
        child1 = new Human("T", "Z", 2020);
    }

    @Test
    public void addChild() {
        family.addChild(child1);
        assertEquals(1, family.getChildren().length);
    }

    @Test
    public void deleteChild() {
        family.addChild(child1);
        family.deleteChild(0);
        assertEquals(0,family.getChildren().length);
    }
    @Test
    public void deleteChild1(){
        family.addChild(child1);
        family.deleteChild(child1);
        assertEquals(0,family.getChildren().length);
    }

    @Test
    public void countFamily() {
        family.addChild(child1);
        assertEquals(3,family.countFamily());
    }
}
