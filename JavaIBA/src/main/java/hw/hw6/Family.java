package hw.hw6;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        children = new Human[0];
    }

    public void addChild(Human child) {
        Human[] temp = new Human[children.length + 1];
        for (int i = 0; i < children.length; i++) {
            temp[i] = children[i];
        }

        temp[children.length] = child;
        children = temp;
    }

    public boolean deleteChild(int idx) {
        int temp_i = 0;
        if (idx < 0 || idx > this.children.length - 1)
            return false;
        Human[] temp = new Human[children.length - 1];
        for (int i = 0; i < children.length; i++) {
            if (i != idx) {
                temp[temp_i] = children[i];
                temp_i++;
            }
        }
        children = temp;
        return true;
    }

    public boolean deleteChild(Human child) {
        for (int i = 0; i < getChildren().length; i++) {
            if (children[i].equals(child)) {
                deleteChild(i);
                break;
            }
        }
        return false;
    }

    public int countFamily() {
        return children.length + 2;
    }

    public void describePet() {
        String sly = pet.trickLevel > 50 ? "very sly" : "not almost sly";
        System.out.println("I have a " + pet.species + " he is " + pet.age + ", he is " + sly);
    }

    public Human[] getChildren() {
        return children;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Inside Family class- Collecting Garbage");
        System.gc();
    }
}
