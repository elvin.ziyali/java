package hw.hw6;

import java.util.Arrays;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule;
    private Family family;

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        schedule = new String[7][2];

    }

    public Human() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void addFamily(Family family) {
        this.family = family;
    }

    public String getScheduleString() {
        String result = "schedule=[";
        for (int i = 0; i < schedule.length; i++) {
            result += "[" + schedule[i][0] + " , " + schedule[i][1] + "]";
        }
        result += "]";
        return result;
    }


    public void setYear(int year) {
        this.year = year;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }


    public int getIq() {
        return iq;
    }


    public int getYear() {
        return year;
    }


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    @Override
    public int hashCode() {
        int hash = this.year;
        hash = 13 * hash + this.year;
        hash = 13 * hash + this.name.length();
        hash = 13 * hash + this.surname.length();
        hash = 13 * hash + this.schedule.length;
        return hash;
    }


    public boolean equals(Human other) {
        if (this.name.equals(other.name)
                && this.surname.equals(other.surname)
                && this.year == other.year
                && this.iq == other.iq
                && Arrays.equals(this.schedule, other.schedule)) {
            return true;
        } else
            return false;
    }

    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;


    }
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Inside Human class- Collecting Garbage");
        System.gc();
    }

    @Override
    public String toString() {
        return "Human{name='" + this.name + "', surname='" + this.surname + "', year = " + this.year + ", iq=" + this.iq + ", " + this.getScheduleString() + "}";
    }


}
