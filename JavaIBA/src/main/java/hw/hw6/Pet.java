package hw.hw6;

import java.util.Arrays;


public class Pet {
    public Species species;
    public String nickname;
    public int trickLevel;
    public int age;
    public String[] habits;

    @Override
    public int hashCode() {
        return super.hashCode();
    }


    public boolean equals(Pet other) {
        if (this.species.equals(other.species)
                && this.age == other.age
                && this.trickLevel == other.trickLevel
                && Arrays.equals(this.habits, other.habits))
            return true;
        return false;
    }

    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
        this.habits = new String[0];
    }

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {
        this.habits = new String[0];

    }

    public void eat() {
        System.out.println("I am eating");
    }

    public void respond() {
        System.out.println("Hello owner, I am " + this.nickname + ". I miss you");
    }

    public void foul() {
        System.out.println("I need to cover up");
    }
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Inside Pet class- Collecting Garbage");
        System.gc();
    }

    @Override
    public String toString() {
        return getString(habits, this.species, this.nickname, this.age, this.trickLevel);
    }

    public static String getString(String[] habits, Species species, String nickname, int age, int trickLevel) {
        String habit = ", habits=[ ";
        for (int i = 0; i < habits.length; i++)
            habit = habit + habits[i] + (i + 1 < habits.length ? ", " : "");
        habit += " ]";
        return species + "{nickname = " + nickname + " age=" + age + ", trickLevel" + trickLevel + habit + "}";
    }
}
