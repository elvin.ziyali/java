package hw.hw7;

public final class Man extends Human implements CanReairCar{

    @Override
    public void repairCar() {
        System.out.println("I can repair Car");
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man() {
    }
    public void greetPet(){
        System.out.println("Hello [Man] , "+super.getPet().getNickname());
    }
    public Man(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }
}
