package hw.hw7;

import hw.hw6.Species;

import java.util.Arrays;


public abstract class Pet {

    public String nickname;
    public int trickLevel;
    public int age;
    public String[] habits;

    @Override
    public int hashCode() {
        return super.hashCode();
    }


    public boolean equals(Pet other) {
        if ( this.age == other.age
                && this.trickLevel == other.trickLevel
                && Arrays.equals(this.habits, other.habits))
            return true;
        return false;
    }

    public Pet( String nickname) {

        this.nickname = nickname;
        this.habits = new String[0];
    }

    public Pet( String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {
        this.habits = new String[0];

    }

    public void eat() {
        System.out.println("I am eating");
    }

//    public void respond() {
//        System.out.println("Hello owner, I am " + this.nickname + ". I miss you");
//    }
    public abstract void respond();

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Inside Pet class- Collecting Garbage");
        System.gc();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    public String toString() {
        return getString(habits, this.nickname, this.age, this.trickLevel);
    }

    public static String getString(String[] habits, String nickname, int age, int trickLevel) {
        String habit = ", habits=[ ";
        for (int i = 0; i < habits.length; i++)
            habit = habit + habits[i] + (i + 1 < habits.length ? ", " : "");
        habit += " ]";
        return  "{nickname = " + nickname + " age=" + age + ", trickLevel" + trickLevel + habit + "}";
    }
}
