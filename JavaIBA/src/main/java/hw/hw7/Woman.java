package hw.hw7;

public final class Woman extends Human implements CanMakeup {
    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Woman() {
    }
    public void greetPet(){
        System.out.println("Hi [Woman] , "+super.getPet().getNickname());
    }
    @Override
    public void makeUp() {
        System.out.println("I can do Make Up");
    }
}
