package hw.hw7;

public class DomesticCat extends Pet implements canFoul {
    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Hello owner, I am " + this.nickname + ". I miss you");
    }

    @Override
    public void foul() {
        System.out.println("I need to cover up");
    }
}
