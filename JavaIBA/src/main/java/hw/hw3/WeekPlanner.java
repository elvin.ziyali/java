package hw.hw3;

import java.util.Scanner;

public class WeekPlanner {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String[][] schedule = new String[7][2];
        int weekday;
        String s;
        boolean b = true;
        schedule = setSchedule(schedule);
        while (b) {
            System.out.println("Please, input the day of the week:");
            s = in.nextLine().toLowerCase();
            switch (s) {
                case "sunday":
                    weekday = 0;
                    displaySchedule(weekday, schedule);
                    break;
                case "monday":
                    weekday = 1;
                    displaySchedule(weekday, schedule);
                    break;
                case "tuesday":
                    weekday = 2;
                    displaySchedule(weekday, schedule);
                    break;
                case "wednesday":
                    weekday = 3;
                    displaySchedule(weekday, schedule);
                    break;
                case "thursday":
                    weekday = 4;
                    displaySchedule(weekday, schedule);
                    break;
                case "friday":
                    weekday = 5;
                    displaySchedule(weekday, schedule);
                    break;
                case "saturday":
                    weekday = 6;
                    displaySchedule(weekday, schedule);
                    break;
                case "exit":
                    b = false;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");

            }
        }

    }

    public static void displaySchedule(int weekday, String[][] schedule) {
        System.out.println("Your task for " + schedule[weekday][0] + ": " + schedule[weekday][1]);
    }

    public static String[][] setSchedule(String[][] schedule) {
        schedule[0][0] = "Sunday";
        schedule[1][0] = "Monday";
        schedule[2][0] = "Tuesday";
        schedule[3][0] = "Wednesday";
        schedule[4][0] = "Thursday";
        schedule[5][0] = "Friday";
        schedule[6][0] = "Saturday";

        schedule[0][1] = "do homework";
        schedule[1][1] = "go to course: watch a film";
        schedule[2][1] = "hang out with friends";
        schedule[3][1] = "clean room";
        schedule[4][1] = "do laundry";
        schedule[5][1] = "visit family";
        schedule[6][1] = "read book";

        return schedule;
    }

}
