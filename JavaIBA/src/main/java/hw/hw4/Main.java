package hw.hw4;

public class Main {
    public static void main(String[] args) {
        Pet p1= new Pet();
        Pet p2= new Pet("dog","Rex");
        Pet p3= new Pet("rabbit","Toplan",2,50,new String[]{"jump","run"});
        p2.eat();
        p3.respond();
        p3.foul();
        String[][] schedule= new String[7][2];
        schedule[0][0]="Sunday";
        schedule[1][0]="Monday";
        schedule[2][0]="Tuesday";
        schedule[3][0]="Wednesday";
        schedule[4][0]="Thursday";
        schedule[5][0]="Friday";
        schedule[6][0]="Saturday";

        schedule[0][1]="do homework";
        schedule[1][1]="go to course: watch a film";
        schedule[2][1]="hang out with friends";
        schedule[3][1]="clean room";
        schedule[4][1]="do laundry";
        schedule[5][1]="visit family";
        schedule[6][1]="read book";


        Human h1= new Human();
        Human h2=  new Human("Elvin","Ziyali",1999);

        Human h3= new Human("Mhbb","Cmrd",1999,h1,h1);
        Human h4 = new Human("Terlan","Ziyali",2025,p3,h2,h3,schedule);

        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
        System.out.println(h2);
        System.out.println(h3);
        System.out.println(h4);

    }


}
