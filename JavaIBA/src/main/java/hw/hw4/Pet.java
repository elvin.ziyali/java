package hw.hw4;

public class Pet {
    public String species;
    public String nickname;
    public int trickLevel;
    public int age;
    public String[] habits;

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
        this.habits= new String[0];
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age=age;
        this.trickLevel=trickLevel;
        this.habits=habits;
    }

    public Pet(){
        this.habits= new String[0];

    }

    public void eat() {
        System.out.println("I am eating");
    }

    public void respond() {
        System.out.println("Hello owner, I am " + this.nickname + ". I miss you");
    }

    public void foul() {
        System.out.println("I need to cover up");
    }



}
