package hw.hw4;

public class Human {
    public String name;
    public String surname;
    public int year;
    public int iq;
    public Pet pet;
    public Human mother;
    public Human father;
    public String[][] schedule;

    public Human(String name, String surname, int year){
        this.name=name;
        this.surname=surname;
        this.year=year;
        this.father= new Human();
        this.mother= new Human();
        this.pet= new Pet();
    }
    public Human(){
    }

    public Human(String name, String surname, int year, Human father,Human mother){
        this.name=name;
        this.surname=surname;
        this.year=year;
        this.father=father;
        this.mother=mother;
        this.pet=new Pet();

    }
    public Human(String name, String surname, int year,Pet pet,Human father,Human mother,String[][]schedule){
        this.name=name;
        this.surname=surname;
        this.year=year;
        this.pet=pet;
        this.father=father;
        this.mother=mother;
        this.schedule=schedule;
    }
    public void greetPet(){
        System.out.println("Hello, "+pet.nickname);
    }
    public void describepet(){
        String sly = pet.trickLevel>50 ? "very sly" : "not almost sly";
        System.out.println("I have a "+pet.species+" he is "+ pet.age+ ", he is " + sly );
    }

    @Override
    public String toString() {
        return "Human{name'"+this.name+"', surname='"+this.surname+"', year = "+ this.year+ ", iq="+this.iq+", mother = "+ this.mother.name+" "+this.mother.surname +", father= "+this.father.name+" "+ this.father.surname+" pet=" +pet.toString()+"}";
    }


}
