package hw.hw5;

import java.util.ArrayList;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        children = new Human[0];
    }

    public void addChild(Human child) {
        Human[] temp = new Human[children.length + 1];
        for (int i = 0; i < children.length; i++) {
            temp[i] = children[i];
        }

        temp[children.length - 1] = child;
        children = temp;
    }

    public boolean deleteChild(int idx) {
        if (idx < 0 || idx > this.children.length-1)
            return false;
        Human[] temp = new Human[children.length - 1];
        for (int i = 0; i < children.length; i++) {
            if (idx != i)
                temp[i] = children[i];
        }
        return true;
    }

    public int countFamily() {
        return children.length + 2;
    }

    public void describePet() {
        String sly = pet.trickLevel > 50 ? "very sly" : "not almost sly";
        System.out.println("I have a " + pet.species + " he is " + pet.age + ", he is " + sly);
    }

    public Human[] getChildren() {
        return children;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
