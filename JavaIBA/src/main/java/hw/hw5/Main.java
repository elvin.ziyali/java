package hw.hw5;

import hw.hw3.WeekPlanner;

public class Main {
    public static void main(String[] args) {
        Human mother = new Human("M", "C", 1999);
        Human father = new Human("E", "Z", 1999);
        Family family = new Family(mother, father);
        Human child1 = new Human("T", "Z", 2020);
        String[][] schedule = new String[7][2];
        WeekPlanner.setSchedule(schedule);
        family.addChild(child1);
        family.addChild(child1);
        family.addChild(child1);
        System.out.println(family.countFamily());
        family.deleteChild(0);
        System.out.println(family.countFamily());
        //System.out.println(family.getChildren());

    }
}
