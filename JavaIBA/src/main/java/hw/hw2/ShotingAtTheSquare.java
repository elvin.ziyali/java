package hw.hw2;

import java.util.Random;
import java.util.Scanner;

public class ShotingAtTheSquare {
    public static void main(String[] args) {
        Random r= new Random();
        Scanner in = new Scanner(System.in);
        boolean b=false;
        char[][] map= new char[6][6];
        int targetX=r.nextInt(4)+1;
        int targetY=r.nextInt(4)+1;
        for (int i = 0; i < map.length; i++) {
            map[0][i]=(char)(i+'0');
            map[i][0]=(char)(i+'0');
        }
        for (int i = 1; i < map.length  ; i++) {
            for (int j = 1; j < map.length; j++) {
                map[i][j]='-';
            }
        }
        System.out.println("All the set get ready for random");
        while(!b){
            int x,y;
            do{
                System.out.println("Enter point X (between 1 and 5");
                x=in.nextInt();}while(x<1 && x>5);
            do{
                System.out.println("Enter point Y (between 1 and 5");
                y=in.nextInt();}while(y<1 && y>5);
            b=isGameEnd(x, y, targetX, targetY, map);


        }


    }
    public static void displayMap(char [][] map){
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                System.out.print(map[i][j]+" | ");
            }
            System.out.println();

        }
    }

    public static boolean isGameEnd(int x,int y,int targetX,int targetY,char[][] map){
        if(targetX==x && targetY==y){
            map[x][y]='x';
            displayMap(map);
            System.out.println("You won");
            return true;}
        else {
            map[x][y]='*';
            displayMap(map);
            return false;}
    }
}
