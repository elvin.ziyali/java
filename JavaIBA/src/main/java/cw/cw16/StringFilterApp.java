package cw.cw16;

import java.util.StringJoiner;

public class StringFilterApp {
    public static void main(String[] args) {
        String origin="Baku";
        String filtered = filter(origin);
        System.out.println(filtered);
    }
    public static String filter(String origin){
        String filtered="";
        for (int i = 0; i < origin.length() ; i++) {
            switch (origin.charAt(i)){
                case 'A': case 'a': case 'o': case 'O': case 'u': case 'U': case 'i':case 'I': case 'e': case 'E': break;
                default:
                    filtered+=origin.charAt(i);
            }
        }
        return filtered;
    }
}
