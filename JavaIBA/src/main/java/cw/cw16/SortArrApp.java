package cw.cw16;

import java.lang.reflect.Array;
import java.util.Arrays;

public class SortArrApp {
    public static void main(String[] args) {
        int[] arr={5,8,3,0,1,6};
        int [] sorted = sort(arr);
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(sorted));
    }

    public static int[] sort(int[] arr) {
        int[] sorted = arr.clone();

        for (int i = 0; i < sorted.length; i++) {
            int min_idx = i;
            for (int j = 0; j < sorted.length; j++) {
                if (sorted[j] < sorted[min_idx])
                    min_idx = j;
            }
            int temp = sorted[min_idx];
            sorted[min_idx] = sorted[i];
            sorted[i] = temp;
        }
        return sorted;
    }
    public static int[] sort2(int[] arr){
        return null;
    }
}
