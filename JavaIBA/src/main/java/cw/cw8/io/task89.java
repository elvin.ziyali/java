package cw.cw8.io;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Stream;

public class task89 {
    public static void main(String[] args) throws IOException {
        File file = new File("data/_123.txt");
        File file2 = new File("data/task89.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        BufferedWriter bw2 = new BufferedWriter(new FileWriter(file2));

        BufferedReader br = new BufferedReader(new FileReader(file));
        ArrayList<String> strings = new ArrayList<String>() {{
            for (int i = 0; i < 33; i++)
                add(Source.random_string(20 + (int) (Math.random() * 10)));
        }};


        strings.forEach(s -> {
            try {
                bw.write(s);
                bw.newLine();
            } catch (IOException ignored) {}
        });
        bw.close();
        Stream<String> lines = br.lines();
        lines.sorted((o1, o2) -> o1.length() - o2.length()).forEach(s -> {
            try {
                bw2.write(s);
                bw2.newLine();
            } catch (IOException ignored) {}
        });
        bw2.close();

    }
}
