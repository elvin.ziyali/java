package cw.cw8.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class IOFileWriterApp {
    public static void main(String[] args) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File("data/_123.txt")));
        bw.write("hello I found it");
        bw.close();
    }
}
