package cw.cw8.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.stream.Stream;

public class IOFileReadApp {
    public static void main(String[] args) throws IOException {
        String fileName = "myFile.txt";
        File file = new File(fileName);
        FileReader fr = new FileReader(file);

        Scanner scanner = new Scanner(fr);
        String str = scanner.nextLine();

        BufferedReader br = new BufferedReader(fr);
        Stream<String> contents = br.lines();
        contents.forEach(s-> System.out.println(s));
    }
}
