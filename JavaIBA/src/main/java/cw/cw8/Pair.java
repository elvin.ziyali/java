package cw.cw8;

import java.util.ArrayList;

public class Pair {
    public int indexL,indexR,sum;
    public Pair(int indexL,int indexR){
        this.indexL=indexL;
        this.indexR=indexR;
    }
    public Pair(){}
    public Pair getPair(ArrayList<Integer> list){
        Pair p= new Pair();
        if(list.size()<2){

            p.indexL=-1;
            p.indexR=-1;
            return p;
        }
        p.sum=list.get(0)+list.get(1);
        for(int i=1;i<list.size()-1;i++){
            if(list.get(i)+list.get(i+1)<p.sum) {
                p.sum = list.get(i) + list.get(i + 1);
                p.indexR=i+1;
                p.indexL=i;
            }
        }
        return p;

    }
}
