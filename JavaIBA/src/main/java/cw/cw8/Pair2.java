package cw.cw8;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.IntFunction;
import java.util.stream.IntStream;

public class Pair2 {
    int index,sum;
    public Pair2(int index,int sum){
        this.index=index;
        this.sum=sum;
    }
    public Pair2(int index){
        this.index=index;
    }
    public Pair2(){
        this(-1,Integer.MAX_VALUE);
    }
    public Pair2 getPair(ArrayList<Integer> list){
        Pair2 pair= IntStream.rangeClosed(0,list.size()-2)
                .mapToObj(index -> new Pair2(index,list.get(index)+list.get(index+1)))
                .min((p1, p2) -> p1.sum-p2.sum).orElse(new Pair2());
        return pair;
    }
}
