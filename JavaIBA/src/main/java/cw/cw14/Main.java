package cw.cw14;

import javax.crypto.spec.PSource;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] src = random(25);
        int[] dst=sort(src);
        display(src);
        display(dst);

    }

    public static int[] random(int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = (int) (Math.random() * 40 + 10);
        }
        return arr;
    }

    public static int[] sort(int[] arr) {
        int [] sorted = arr.clone();
        for (int j = 0; j < sorted.length; j++) {
            for (int i = 0; i < sorted.length-1; i++) {
                if (sorted[i] > arr[i + 1]) {
                    int temp = sorted[i + 1];
                    sorted[i + 1] = sorted[i];
                    sorted[i] = temp;
                }
            }
        }
        return sorted;
    }
    public static void display(int [] arr){
        System.out.println(Arrays.toString(arr));
    }
}
