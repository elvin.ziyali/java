package cw.cw14;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Merge {
    public static void main(String[] args) {
        int a[] = {1, 3, 5, 7, 13};
        int b[] = {2, 4, 6, 20, 40, 100};
        int[] c = merge(a, b);
        System.out.println(Arrays.toString(c));

    }

    public static int[] merge(int[] arr1, int[] arr2) {
        int[] data = new int[arr1.length + arr2.length];
        int i = 0, j = 0, k = 0;
        while (i < arr1.length && j < arr2.length) {
            if (arr1[i] < arr2[j]) {
                data[k] = arr1[i];
                i++;
            } else {
                data[k] = arr2[j];
                j++;
            }
            k++;
        }

        while (i < arr1.length) data[k++] = arr1[i++];

        while (j < arr2.length) data[k++] = arr2[j++];

        return data;
    }
}
