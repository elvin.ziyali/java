package cw.cw14;

public class Covert {
    public static void main(String[] args) {
        System.out.println(Conv2Bin(7));
        System.out.println(Conv2Dec("111"));
    }


    public static String Conv2Bin(int n){
        String s="";
        for (int i=n;i>0;i=i/2){
            s+=i%2;
        }
        return s;
    }


    public static int Conv2Dec(String s){
        int n=0;
        for (int i=s.length();i>0;i--){
            if(s.charAt(i-1)=='1'){
                n++;
            }
            n=n<<1;
        }
        return n>>1;
    }
}
