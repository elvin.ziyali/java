package cw.cw5.iterator;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;


public class HiddenData implements Iterable<String> {
  private final List<String> months = Arrays.asList(
      "Jan","Feb","Mar","Apr","May");

  @Override
  public Iterator<String> iterator() {
    Random r = new Random();
    Iterator<String> iterator = new Iterator<String>() {

      int index =r.nextInt(months.size()-1);

      @Override
      public boolean hasNext() {
        return index > 0;
      }

      @Override
      public String next() {
        return months.get(index--);
      }
    };

    return iterator;
  }
}
