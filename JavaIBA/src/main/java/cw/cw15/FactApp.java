package cw.cw15;

public class FactApp {
    public static void main(String[] args) {
        FactApp app = new FactApp();
        int f= app.fact(6);
        System.out.println(f);
    }

    private int fact(int i) {
        if(i==0 || i==1)  return 1;

        return i*fact(i-1);
    }
}
