package cw.cw17.Web;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FilereaderApp {

    private List<String> result;
    private String path;

    public FilereaderApp(String path) {
        this.path = path;
        result= new ArrayList<>();
    }

    public List<String> get() {
        File file;
        try{
         file = new File(path);
         FileReader fileReader = new FileReader(file);
         BufferedReader br = new BufferedReader(fileReader);
            result=br.lines().collect(Collectors.toList());
            br.close();
            }
        catch (Exception e){
            result.add("File Not Found");
            return result;
        }
        if(result.size()==0)
            result.add("File is empty");
        return result;



    }
}
