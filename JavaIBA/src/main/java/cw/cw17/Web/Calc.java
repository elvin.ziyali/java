package cw.cw17.Web;

public class Calc {

    public String calc(String s1, String  s2, String op) {
        int a,b;
        try {
            a= Integer.parseInt(s1);
        }catch (Exception e){
            return "Invalid input for a";
        }
        try{
            b=Integer.parseInt(s2);
        }catch (Exception e){
            return "Invalid input for b";
        }
        switch (op) {
            case "add":
                return String.valueOf(a + b);
            case "sub":
                return String.valueOf(a - b);
            case "mult":
                return String.valueOf(a * b);
            case "div":
                if (b == 0) {
                    return "You are trying to divide by 0 !!!";
                }
                return String.valueOf(a / b);
            default:
                return "Invalid operation";

        }
    }
}
