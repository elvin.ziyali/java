package cw.cw17.Web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class FileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getPathInfo();
        FilereaderApp filereaderApp = new FilereaderApp(path);
        List<String > result= filereaderApp.get();
        try (PrintWriter writer = resp.getWriter()) {
            writer.println(result.toString());
        }
    }
}
