package cw.cw17.graph;

import java.util.ArrayList;
import java.util.List;

public class XGraphPath {
    private final XGraph g;

    public XGraphPath(XGraph g) {
        this.g = g;
    }

    List<Integer> path(int src, int dst) {
        ArrayList<Integer> path = new ArrayList<>();
        if(check(src,dst,new ArrayList<Integer>())){}

        return path;
    }



    boolean check(int src, int dst,List<Integer>path) {
        if (g.check(src, dst)) return true;

        List<Integer> destinations = g.get(src);
        for (Integer src1 : destinations) {
            check(src1, dst,path);
        }
        return false;
    }

}
