package cw.cw17.Binarytree;

public class BinaryTree {
    Node root;

    void add(int value) {
        root= add(root,value);
    }

    Node add(Node curr, int value){
        if(curr == null){
            return new Node(value);
        }
        if (value<curr.value){
            curr.left=add(curr.left,value);
        }
        else if(value>curr.value){
            curr.right=add(curr.right,value);
        }
        return curr;
    }
//    void add(Node curr, int value) {
//        if (value < curr.value)
//            if(curr.left==null)
//                curr.left=new Node(value);
//            else
//                add(curr.left,value);
//        else if (value > curr.value)
//            if(curr.right==null)
//                curr.right= new Node(value);
//            else
//                add(curr.right, value);
//        return;
//
//    }



    boolean contains(int value) {
        return contains(root, value);
    }

    boolean contains(Node curr, int value) {
        if (curr == null)
            return false;
        if (curr.value==value)
            return true;
        return value < curr.value ? contains(curr.left, value) : contains(curr.right, value);

    }
}
