package cw.cw17;

public class PrimeNumberApp {
    public static void main(String[] args) {
        System.out.println(check(6));
    }
    static boolean check(int origin){
        boolean f =true;
        return check(2,origin,f);
    }
    static boolean check (int num,int origin,boolean f){
        if(!f || num>=origin) return f;
        if(origin%num==0)
            f=false;
        return check(num+1,origin,f);

    }
}
