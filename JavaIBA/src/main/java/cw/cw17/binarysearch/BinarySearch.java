package cw.cw17.binarysearch;

public class BinarySearch {
    public boolean search(int[] origin, int number) {
        return search(origin, number, 0, origin.length - 1);
//    if(number>origin[origin.length/2])
//    return search(origin,number,0,origin.length/2);
//    return search(origin,number,origin.length/2,origin.length);

    }

    public boolean search(int[] origin, int number, int start, int end) {
        int m = (start + end) / 2;
        if (start >= end) return false;
        if (number == origin[m]) return true;
        if (number > origin[m])
            return search(origin, number, start, m);
        else
            return search(origin, number, m, end);
    }
}
